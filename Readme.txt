Kubernetes Kurulumu
- google cloudan 1 tane ubuntu sunucu oluşturularak içerisine tek node lu kubernetes kuruldu.

- kubectl init yaptıktan sonra konteynerler arası ve host arası ağ iletişimi sağlamak için weave net kuruldu. 
	#kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

- dışardan uygulamaya erişim ayarlarının yapılabilmesi için nginx ingress kuruldu.
	#kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.2/deploy/static/provider/cloud/deploy.yaml
- IP ataması yapması için MetalLB kurulumu yapıldı.
	#kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/namespace.yaml
	#kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/metallb.yaml
	#kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
-mysql için db namespace i oluşturuldu. ingress tanımı yapıldı. Nodeport (31111) verildi. Kuberneteste çalışır hale getirildi.
	#kubectl apply -f db-namespace.yaml
	#kubectl apply -f mysql-pv.yaml
	#kubectl apply -f mysql-pvc.yaml
	#kubectl apply -f deployment-mysql.yaml
	#kubectl apply -f svc-mysql.yaml
	#kubectl apply -f ingress_db.yaml
-app python servisinin çalışması için gerekli yaml çalıştırıldı. Nodeport verildi.(30085). Kuberneteste 8585 portunda çalışıyor.
	#kubectl apply -f app-namespace.yaml
	#kubectl apply -f hellopy-pv.yaml
	#kubectl apply -f hellopy-pvc.yaml
	#kubectl apply -f hellopy-deployment.yaml
	#kubectl apply -f hellopy-svc.yaml
	
Kubernetes kurulu sunucu üzerine gitlab-runner kuruldu. Gerekli ayarlar yapılarak Kendi gitlab im ile bağlantı sağlandı.

Gitlab pipline çalışmak üzere 3 stage eklendi.
.gitlab-ci.yml dosyası içerisinde build - test ve deploy satageleri oluşturulmuştur. Gitlab a commit olduğu zaman pipline çalışmaya başalıyor.
Build stage inde yeni imagı oluşturup pushluyor. Test satage in de bağlantıları kontrol ediyor ve son aşama deploy satage inde
kubenetes podu nu öldürüp tekrar ayağa kalıyor ve proje güncellenmiş oluyor.

Dışarıdan projeye erişmek için Nodeport tanımladım. Aşağıdaki şekilde erişimini sağladım.
http://34.159.246.171:30085/

Gitlab adresi = https://gitlab.com/husoyargic/pythonhello.git


	