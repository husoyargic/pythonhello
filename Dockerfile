FROM python:3.9

WORKDIR usr/src/flask_app
COPY requirements.txt .
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
CMD gunicorn -w 1 -b 0.0.0.0:8585 main:app
